#!/usr/bin/env python
import sys
import time
import math
import random

def compute_pi(
    N = 1000,
    ):

    M = 0
    for k in xrange(N):
        x = random.random()
        y = random.random()
        if x**2 + y**2 <= 1.0:
            M += 1
    pival = 4.0 * M / N 
    return pival

def test_pi():

    if len(sys.argv) != 2:
        print 'Usage: ./pi1.py Nsample'
        return 

    N = int(sys.argv[1])
    
    start = time.time()
    pival = compute_pi(N)
    elapsed = time.time() - start    
    print 'Python: Estimate: %18.16f, %.3f s' % (pival, elapsed)
    
if __name__ == '__main__':
    test_pi()
