#include <boost/random/random_device.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_real_distribution.hpp>

double compute_pi(size_t N)
{
    boost::random::random_device seed;
    boost::random::mt19937 mt(seed);
    boost::random::uniform_real_distribution<double> range(0.0,1.0);

    size_t M = 0;
    for (size_t k = 0; k < N; k++) {
        double x = range(mt); 
        double y = range(mt); 
        if (x*x + y*y < 1.0) {
            M++;
        }
    }
    return (4.0 * M) / N;
}

#include <boost/python.hpp>
using namespace boost::python;

BOOST_PYTHON_MODULE(pi)
{
    def("compute_pi", &compute_pi);
}
