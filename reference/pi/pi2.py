#!/usr/bin/env python
import sys
import time
import math

# Import our C++ code
import pi

def test_pi():

    if len(sys.argv) != 2:
        print 'Usage: ./pi2.py Nsample'
        return 

    N = int(sys.argv[1])
    
    start = time.time()
    pival = pi.compute_pi(N)
    elapsed = time.time() - start    
    print 'C++:    Estimate: %18.16f, %.3f s' % (pival, elapsed)
    
if __name__ == '__main__':
    test_pi()
