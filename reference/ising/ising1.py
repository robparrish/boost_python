#!/usr/bin/env python
import time
import math
import random
import numpy as np

# Trick needed if not -X forwarding
import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt

# ==> Ising2D Code <== #

class Ising2D(object):

    # => Initialization <= #

    def __init__(
        self,
        Nx = 32,
        Ny = 32,
        J = 1.0,
        beta = 1.0,
        ):

        self.Nx = Nx
        self.Ny = Ny
        self.J = J
        self.beta = beta

        # Initialize spin lattice to all up
        self.lattice = np.ones((self.Nx,self.Ny),dtype=np.int)

    # => Guesses <= #

    def guess(
        self,
        guess,
        ):

        self.lattice[...] = guess

    def guess_all_up(
        self,
        ):
    
        self.guess(+1)

    def guess_random(
        self,
        ):

        lattice = np.ones((self.Nx,self.Ny),dtype=np.int)
        lattice[np.random.rand(self.Nx,self.Ny) > 0.5] = -1
        self.guess(lattice)

    # => Monte Carlo <= #

    def simulate(
        self,
        nT = 1000,
        ):

        # > Initialization < #

        # Initial total magnetization
        M = np.sum(self.lattice)

        # Initial total energy
        E = 0.0
        for i in range(self.Nx):
            for j in range(self.Ny):
                E -= self.J * self.lattice[i,j] * ( \
                    self.lattice[(i-1) % self.Nx,j] + \
                    self.lattice[(i+1) % self.Nx,j] + \
                    self.lattice[i,(j-1) % self.Ny] + \
                    self.lattice[i,(j+1) % self.Ny])

        # Observable registers
        Ms = np.zeros((nT,),dtype=np.double)
        Es = np.zeros((nT,),dtype=np.double)

        # > Main loop in time < #
    
        for T in xrange(nT):

            # > Observables < #
            
            # Save observables for this timestep
            Ms[T] = M
            Es[T] = E
            
            # > Monte Carlo Step < #

            # Propose a random index
            i = random.randint(0,self.Nx-1)
            j = random.randint(0,self.Ny-1)

            # Figure out the magnetization change if flipped
            dM = -2 * self.lattice[i,j]
            
            # Figure out the energy change if flipped 
            dE = +2.0 * self.J * self.lattice[i,j] * ( \
                self.lattice[(i-1) % self.Nx,j] + \
                self.lattice[(i+1) % self.Nx,j] + \
                self.lattice[i,(j-1) % self.Ny] + \
                self.lattice[i,(j+1) % self.Ny])

            # Use the Metropolis condition to decide to flip the spin or not
            if dE < 0.0 or math.exp(-self.beta * dE) > random.random():
                M += dM
                E += dE
                self.lattice[i,j] *= -1

        # Return a dict of observables over timesteps
        return {
            'M' : Ms,
            'E' : Es,
            }

# ==> Ising2D Test Routines <== #

def testIsing2D():

    # Problem specification
    Nx = 32
    Ny = 32
    J = 1.0

    # Temperature traverse (independent)
    Ts = np.array([1.00, 1.25, 1.50, 1.75, 2.00, 2.25, 2.50, 2.75, 3.00, 3.25, 3.50])
    # Magnetization (dependent)
    ms = np.zeros_like(Ts)

    # Number of Monte Carlo samples for each temperature
    Ns = { T : 1000000 for T in Ts}

    # Ising2D Monte Carlo runs
    print 'Ising2D Runs:'
    for k, T in enumerate(Ts):
        isi = Ising2D(Nx, Ny, J, 1. / T)
        isi.guess_all_up()
        print 'T = %4.2f: %11d steps: ' % (T, Ns[T]), 
        start = time.time()
        vals = isi.simulate(Ns[T])
        elapsed = time.time() - start
        print '%7.3f s' % elapsed
        ms[k] = np.mean(vals['M'][len(vals['M']) / 2:] / float(isi.Nx * isi.Ny))

    # Onsager's results
    Tc = 2. / math.log(1 + math.sqrt(2.))
    T2s = np.linspace(min(Ts),max(Ts),1000)
    m2s = (1. - np.sinh(2. / T2s)**(-4.))**(1./8.)
    m2s[T2s > Tc] = 0.0

    # Magnetization plot
    plt.clf()
    plt.plot(Ts,ms,'-xb')
    plt.plot(T2s,m2s,'-k')
    plt.xlabel(r'$T$')
    plt.ylabel(r'$\langle m_i \rangle$')
    plt.legend(['Ising2D', 'Onsager'], loc=1)
    plt.savefig('m1.pdf')
        
if __name__ == '__main__':    
    testIsing2D()
