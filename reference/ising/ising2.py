#!/usr/bin/env python
import time
import math
import numpy as np

# Trick needed if not -X forwarding
import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt

# Import 
import ising

# ==> Dress up ising.Ising2D <== #

# => Guesses <= #

def _Ising2D_guess(
    self,
    guess, # Some sort of 2D array assignable
    ):

    lattice2 = np.zeros((self.Nx,self.Ny),dtype=np.int)
    lattice2[...] = guess
    self.py_guess(lattice2.ravel())

def _Ising2D_guess_all_up(
    self,
    ):

    self.guess(+1)

def _Ising2D_guess_random(
    self,
    ):

    lattice = np.ones((self.Nx,self.Ny),dtype=np.int)
    lattice[np.random.rand(self.Nx,self.Ny) > 0.5] = -1
    self.guess(lattice)

def _Ising2D_simulate(
    self,
    nT = 1000,
    ):
    
    vals = self.py_simulate(nT)

    return {
        'M' : np.array(vals['M']),
        'E' : np.array(vals['E']),
        }

@property
def _Ising2D_lattice(
    self,
    ):

    return np.reshape(self.py_lattice,(self.Nx,self.Ny))
 
ising.Ising2D.guess = _Ising2D_guess
ising.Ising2D.guess_all_up = _Ising2D_guess_all_up
ising.Ising2D.guess_random = _Ising2D_guess_random
ising.Ising2D.simulate = _Ising2D_simulate
ising.Ising2D.lattice = _Ising2D_lattice

# ==> Ising2D Test Routines <== #

def testIsing2D():

    # Problem specification
    Nx = 32
    Ny = 32
    J = 1.0

    # Temperature traverse (independent)
    Ts = np.array([1.00, 1.25, 1.50, 1.75, 2.00, 2.25, 2.50, 2.75, 3.00, 3.25, 3.50])
    # Magnetization (dependent)
    ms = np.zeros_like(Ts)

    # Number of Monte Carlo samples for each temperature
    Ns = { T : 1000000 for T in Ts}

    # Ising2D Monte Carlo runs
    print 'Ising2D Runs:'
    for k, T in enumerate(Ts):
        isi = ising.Ising2D(Nx, Ny, J, 1. / T)
        isi.guess_all_up()
        print 'T = %4.2f: %11d steps: ' % (T, Ns[T]), 
        start = time.time()
        vals = isi.simulate(Ns[T])
        elapsed = time.time() - start
        print '%7.3f s' % elapsed
        ms[k] = np.mean(vals['M'][len(vals['M']) / 2:] / float(isi.Nx * isi.Ny))

    # Onsager's results
    Tc = 2. / math.log(1 + math.sqrt(2.))
    T2s = np.linspace(min(Ts),max(Ts),1000)
    m2s = (1. - np.sinh(2. / T2s)**(-4.))**(1./8.)
    m2s[T2s > Tc] = 0.0

    # Magnetization plot
    plt.clf()
    plt.plot(Ts,ms,'-xb')
    plt.plot(T2s,m2s,'-k')
    plt.xlabel(r'$T$')
    plt.ylabel(r'$\langle m_i \rangle$')
    plt.legend(['Ising2D', 'Onsager'], loc=1)
    plt.savefig('m2.pdf')
        
if __name__ == '__main__':    
    testIsing2D()
