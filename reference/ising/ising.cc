#include "ising.h"
#include <cmath>
#include <boost/random/random_device.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_real_distribution.hpp>
#include <boost/random/uniform_int.hpp>

namespace ising {

Ising2D::Ising2D(
    size_t Nx,
    size_t Ny,
    double J,
    double beta) :
    Nx_(Nx),
    Ny_(Ny),
    J_(J),
    beta_(beta)
{
    // Initialize all up
    lattice_ = std::vector<int>(Nx_*Ny_,1);
}
void Ising2D::guess(
    const std::vector<int>& lattice) 
{
    lattice_ = lattice;
}
std::map<std::string, std::vector<double>> Ising2D::simulate(
    size_t nT)
{
    // > Initialization < //
    
    // Random number generator
    boost::random::random_device seed;
    boost::random::mt19937 mt(seed);
    boost::random::uniform_real_distribution<double> range(0.0,1.0);
    boost::uniform_int<int> xrange(0, Nx_-1);
    boost::uniform_int<int> yrange(0, Ny_-1);

    // Inital total magnetization
    double M = 0;
    for (int s : lattice_) {
        M += s;
    }    

    // Initial total energy
    double E = 0.0;
    for (int i = 0; i < Nx_; i++) {
        for (int j = 0; j < Ny_; j++) {
            E -= J_ * lattice_[i*Ny_+j] * (
                lattice_[((i-1) % Nx_) * Ny_ + j] +
                lattice_[((i+1) % Nx_) * Ny_ + j] +
                lattice_[i * Ny_ + ((j-1) % Ny_)] +
                lattice_[i * Ny_ + ((j+1) % Ny_)]);
        }
    }
    
    // Observable registers
    std::vector<double> Ms(nT);
    std::vector<double> Es(nT);

    // > Main loop in time < //
    
    for (size_t T = 0; T < nT; T++) {
        
        // > Observables < //
        
        // Save observables for this timestep
        Ms[T] = M;       
        Es[T] = E;       
 
        // > Monte Carlo Step < //
        
        // Propose a random index
        int i = xrange(mt); 
        int j = yrange(mt); 

        // Figure out the magnetization change if flipped
        double dM = -2 * lattice_[i * Ny_ + j];

        // Figure out the energy change if flipped
        double dE = +2.0 * J_ * lattice_[i * Ny_ + j] * ( 
            lattice_[((i-1) % Nx_) * Ny_ + j] +
            lattice_[((i+1) % Nx_) * Ny_ + j] +
            lattice_[i * Ny_ + ((j-1) % Ny_)] +
            lattice_[i * Ny_ + ((j+1) % Ny_)]);

        if (dE < 0.0 || exp(-beta_ * dE) > range(mt)) {
            M += dM;
            E += dE;
            lattice_[i * Ny_ + j] *= -1;
        }
    }

    // Return a map of observables over timesteps
    std::map<std::string, std::vector<double>> vals;
    vals["M"] = Ms;
    vals["E"] = Es;
    return vals;
}

} // namespace ising
