#ifndef TUT1_ISING_H
#define TUT1_ISING_H

#include <cstddef>
#include <vector>
#include <map>
#include <string>

namespace ising {

class Ising2D {

public:

    // > Constructors < //
    
    Ising2D(
        size_t Nx = 32,
        size_t Ny = 32,
        double J = 1.0,
        double beta = 1.0);

    // > Accessors < //

    size_t Nx() const { return Nx_; }
    size_t Ny() const { return Ny_; }
    double J() const { return J_; }
    double beta() const { return beta_; }

    const std::vector<int>& lattice() const { return lattice_; }
    
    // > Computational routines < //
    
    void guess(
        const std::vector<int>& lattice);

    std::map<std::string, std::vector<double>> simulate(
        size_t nT = 1000);

protected:

    size_t Nx_;
    size_t Ny_;
    double J_;
    double beta_;

    std::vector<int> lattice_;

};

} // namespace ising 

#endif // header guard
